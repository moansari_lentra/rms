package com.ma;

import com.ma.logic.RestaurantControl;
import com.ma.ui.TextUI;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        TextUI text = new TextUI(new RestaurantControl(),new Scanner(System.in));
        text.start();
    }
}
