package com.ma.domain;

public class Bill {
    //private int billId;
    private Customer customer;

    public Bill(Customer customer) {
        //this.billId = billId;
        this.customer = customer;
    }

    public void printBill(){
        Order order = this.customer.getCustomerOrder();
        order.printOrder();
    }

    public double calculateTotal(){
        double total;

        Order order = this.customer.getCustomerOrder();
        total = order.calculateTotalPrice();

        return total;
    }
}
