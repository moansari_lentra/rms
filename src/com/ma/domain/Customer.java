package com.ma.domain;

public class Customer {
    private int customerId;
    private String customerName;
    private Order customerOrder;

    public Customer(){
        this.customerId = 0;
        this.customerOrder = new Order();
    }

    public Customer(int customerId){
        this.customerId = customerId;
        this.customerOrder = new Order(customerId);
    }
    public Customer(int customerId,String customerName){
        this.customerId = customerId;
        this.customerName = customerName;
        this.customerOrder = new Order(customerId);
    }


    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void giveOrder(Item item,int numberOfItems){
        ItemOrder itemOrder = new ItemOrder(item,numberOfItems);
        this.customerOrder.addItemOrder(itemOrder);
    }

    public void giveOrder(ItemOrder itemOrder){
        this.customerOrder.addItemOrder(itemOrder);
    }

    public Order getCustomerOrder() {
        return customerOrder;
    }

    public void setCustomerOrder(Order customerOrder) {
        this.customerOrder = customerOrder;
    }


}
