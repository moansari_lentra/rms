package com.ma.domain;

public class ItemOrder {
    private Item item;
    private int numberOfPlates;

    public ItemOrder(){
        this.item = null;
        this.numberOfPlates = 0;
    }

    public ItemOrder(Item item, int numberOfPlates) {
        this.item = item;
        this.numberOfPlates = numberOfPlates;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public String getItemName(){
        return this.item.getItemName();
    }
    public int getNumberOfPlates() {
        return numberOfPlates;
    }

    public void setNumberOfPlates(int numberOfPlates) {
        this.numberOfPlates = numberOfPlates;
    }

    public double getItemPrice(){
        return this.item.getItemPrice();
    }

    @Override
    public String toString() {
        return this.item.getItemName() + " " + this.numberOfPlates;
    }
}
