package com.ma.domain;

import java.util.ArrayList;

public class Menu {
    private ArrayList<Item> menuList;

    public Menu(){
        this.menuList = new ArrayList<>();
    }

    public void setMenuList(ArrayList<Item> menuList) {
        this.menuList = menuList;
    }

    public ArrayList<Item> getMenuList() {
        return menuList;
    }

    public void printMenu(){
        System.out.println(" ");
        System.out.println("-----MENU-----");
        System.out.printf("%5s   %-20s  %-10s\n","S.No","--Item--","--Price--");
        for(Item i : menuList){
            System.out.printf("%5s   %-20s  %-10s\n",i.getItemId(),i.getItemName(),i.getItemPrice());
        }
        System.out.println(" ");
    }
}
