package com.ma.domain;

import java.util.ArrayList;

public class Order {
    private int orderId;
    private ArrayList<ItemOrder> orderList = new ArrayList<>();

    public Order(){
        this.orderId = 0;
    }

    public Order(int orderId){
        this.orderId = orderId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public ArrayList<ItemOrder> getOrder() {
        return orderList;
    }

    public void setOrder(ArrayList<ItemOrder> order) {
        this.orderList = order;
    }

    public void addItemOrder(ItemOrder item){
        this.orderList.add(item);
    }

    public void removeItemOrder(ItemOrder item){
        this.orderList.remove(item);
    }

    public void printOrder(){
        System.out.println("----BILL DETAILS----");
        System.out.printf("%-30s %-20s %10s\n","----Item----","----Quantity----","----Price----");
        for(ItemOrder i : orderList){
            System.out.printf("%-30s %-20s %-10s\n",i.getItemName(),i.getNumberOfPlates(),i.getItemPrice());
        }
    }

    public double calculateTotalPrice(){
        double total = 0;

        for(ItemOrder i : orderList){
            total += (i.getItemPrice() * i.getNumberOfPlates());
        }

        return total;
    }
}
