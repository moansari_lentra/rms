package com.ma.domain;

public class Table {

    private int tableId;
    private boolean occupied;
    private int numberOfSeats;

    public Table(int tableId, int numberOfSeats){
        this.tableId = tableId;
        this.occupied = false;
        this.numberOfSeats = numberOfSeats;
    }

    public int getTableId() {
        return tableId;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public boolean isOccupied(){
        return occupied;
    }

    public void setOccupied(boolean occupied) {
        this.occupied = occupied;
    }

    public void bookTable(){
        this.occupied = true;
    }

    public void releaseTable(){
        this.occupied = false;
    }

    @Override
    public String toString(){
        return "Table: " + this.tableId + " Occupied: " + this.occupied + " Seats: " + this.numberOfSeats;
    }
}
