package com.ma.logic;

import com.ma.domain.*;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class RestaurantControl {
    private Menu menu ;
    private HashMap<Integer,Customer> cHashMap;
    private ArrayList<Table> tableList;

    public RestaurantControl(){
        //creates menu
        this.menu = new Menu();
        this.menu.setMenuList(createMenu());

        this.cHashMap = new HashMap<>();

        this.tableList = new ArrayList<>();
        this.populateTableList();
    }

    private ArrayList<Item> createMenu(){
        ArrayList<Item> itemList = new ArrayList<>();
        try(Scanner scanner = new Scanner(Paths.get("/home/anas/IdeaProjects/rms/src/com/ma/resources/menu.txt"))){

            while(scanner.hasNextLine()){
                String row = scanner.nextLine();

                if(row.isEmpty())
                    continue;

                String[] parts = row.split(",");

                Item item = new Item(Integer.parseInt(parts[0]), parts[1], Double.parseDouble(parts[2]));

                itemList.add(item);
            }

        }catch(Exception e){
            System.out.println("Error Opening File : " + e.getMessage());
        }
        return itemList;
    }

    public Menu getMenu() {
        return menu;
    }

    public void printMenu() {
        this.menu.setMenuList(createMenu());
        this.menu.printMenu();
    }

    public boolean isTableOccupied(int id){
        boolean a = true;
        for(Table t : this.tableList){
            if(t.getTableId() == id)
                a = t.isOccupied();
        }
        return a;
    }

    private void populateTableList(){

        this.tableList.add(new Table(11,4));
        this.tableList.add(new Table(22,4));
        this.tableList.add(new Table(33,6));

    }
    public void changeTableStatus(int id,boolean status){
        for(Table t : this.tableList){
            if(t.getTableId() == id)
                t.setOccupied(status);
        }
    }
    public void printTableList(){
        for(Table t : this.tableList){
            System.out.println(t);
        }
    }

    public void addCustomer(int tableId){

        cHashMap.put(tableId,new Customer(tableId));
        this.changeTableStatus(tableId,true);
    }

    public void removeCustomer(int tableId){
        cHashMap.remove(tableId);
        changeTableStatus(tableId,false);
    }

    public void giveOrder(int tableId,ItemOrder item){
        Customer c = this.cHashMap.get(tableId);
        //ArrayList<Item> itemList= this.menu.getMenuList();
        c.giveOrder(item);

        this.cHashMap.put(tableId,c);
    }

    public double totalCost(int tableId){
        double amount = 0 ;
        Customer c = this.cHashMap.get(tableId);

        Bill bill = new Bill(c);

        amount = bill.calculateTotal();

        return amount;
    }

    public void printCustomerBill(int tableId){
        Customer c  = this.cHashMap.get(tableId);
        Bill bill = new Bill(c);
        bill.printBill();
    }

    public void addTable(int id,int numberOfSeats){
        this.tableList.add(new Table(id,numberOfSeats));
    }

}
