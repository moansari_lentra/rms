package com.ma.ui;

import com.ma.domain.*;
import com.ma.logic.RestaurantControl;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class TextUI {
    private RestaurantControl rControl;
    private Scanner scanner;

    public TextUI(RestaurantControl rControl, Scanner scanner) {
        this.rControl = rControl;
        this.scanner = scanner;
    }

    public void start(){
        //start with login
        this.login();
    }

    public void login(){
        System.out.println("---LOG IN---");
        System.out.println("");

        while(true){
            System.out.println("1 Admin");
            System.out.println("2 Customer");
            System.out.println("3 Exit");
            System.out.print("> ");

            String input = scanner.nextLine();

            if(input.equals("3"))
                break;
            else if(input.equals("1")){
                this.adminUI();
            }
            else if(input.equals("2")){
                this.customerUI();
            }

        }
    }

    public void adminUI(){

        System.out.println("-----HELLO!----");
        System.out.println("");

        while(true){
            System.out.println("1 Enter Dish");
            System.out.println("2 Add Table");
            System.out.println("3 Exit");
            System.out.print("> ");

            String input = scanner.nextLine();

            if(input.equals("3"))
                break;
            else if(input.equals("1")){
                this.addDish();
            }
            else if(input.equals("2")){
                this.addTableAdmin();
            }
        }

    }

    public void addDish(){
        String filename = "/home/anas/IdeaProjects/rms/src/com/ma/resources/menu.txt";
        try{
            FileWriter fw = new FileWriter(filename,true);
            System.out.print("Enter New Dish ID: ");
            StringBuilder s = new StringBuilder(scanner.nextLine());
            s.append(",");
            System.out.print("Enter Dish Name: ");
            s.append(scanner.nextLine());
            s.append(",");
            System.out.print("Enter Price: ");
            s.append(scanner.nextLine());
            fw.append("\n");
            fw.append(s);
            fw.close();
        } catch(Exception e){
            System.out.println("Error: " + e.getMessage());
        }

    }

    public void addTableAdmin(){
            System.out.print("Enter New Table ID: ");
            int i = Integer.parseInt(scanner.nextLine());
            System.out.print("Enter Number of Seats: ");
            int j = Integer.parseInt(scanner.nextLine());
            this.rControl.addTable(i,j);
    }
    public void customerUI(){
        Customer c =new Customer();

        System.out.println("---HELLO!!---");
        System.out.println("");

        while(true){
            System.out.println("1 Book Table");
            System.out.println("2 Exit");
            System.out.print("> ");

            String input = scanner.nextLine();

            if(input.equals("2"))
                break;
            else if(input.equals("1")){
                printTable();
                System.out.print("Enter TableID: ");
                String check = scanner.nextLine();
                if(this.rControl.isTableOccupied(Integer.parseInt(check))) {
                    System.out.println("Already Booked! ,Enter Again");
                }
                else {
                    //this.rControl.changeTableStatus(Integer.parseInt(check),true);
                    this.rControl.addCustomer(Integer.parseInt(check));
                    this.customerUI2(Integer.parseInt(check));

                }
            }

        }
    }

    public void printTable(){
        this.rControl.printTableList();
    }

    public void customerUI2(int id){

        System.out.println("---HELLO!!---");
        System.out.println("");

        while(true){
            System.out.println("1 Display Menu");
            System.out.println("2 Give Order");
            System.out.println("3 Total");
            System.out.println("4 Bill Details");
            System.out.println("5 Exit");
            System.out.print("> ");

            String input = scanner.nextLine();

            if(input.equals("5")){
                this.rControl.removeCustomer(id);
                break;
            }

            else if(input.equals("1")){
                this.menuUI();
            }
            else if(input.equals("2")){
                this.orderUI(id);
            }
            else if(input.equals("3")){
                System.out.println("Total Cost: " + this.rControl.totalCost(id));
            }
            else if(input.equals("4")){
                this.rControl.printCustomerBill(id);
            }

        }
    }

    public void menuUI(){
        this.rControl.printMenu();
    }

    public void orderUI(int cId){
        while(true)
        {
            System.out.print("Give Item Id: ");
            int item = Integer.parseInt(scanner.nextLine());

            System.out.print("How many: ");
            int quantity = Integer.parseInt(scanner.nextLine());

            this.rControl.giveOrder(cId,new ItemOrder(getMenuItem(item),quantity));

            System.out.println("exit?(y/n)");
            String s = scanner.nextLine();
            if(s.equals("y"))
                break;
            else
                continue;
        }
    }

    public Item getMenuItem(int item){
        Item it = null;
        Menu menu = this.rControl.getMenu();
        for(Item i : menu.getMenuList()){
            if(i.getItemId() == item)
                it = i;
        }
        return it;
    }
}
